package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, length = 128, name = "address_city")
    private String cityAddress;
    @Column(nullable = false, length = 128, name = "address_street")
    private String streetAddress;

    public UserProfile(String cityAddress, String streetAddress) {
        this.cityAddress = cityAddress;
        this.streetAddress = streetAddress;
    }

    public UserProfile() {
    }

    public long getId() {
        return id;
    }

    public String getCityAddress() {
        return cityAddress;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
}
