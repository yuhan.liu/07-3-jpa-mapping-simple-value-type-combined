package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.CompanyProfile;
import com.twuc.webApp.domain.composite.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.UserProfile;
import com.twuc.webApp.domain.composite.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SimpleMappingAndValueTypeTest extends JpaTestBase{
    @Autowired
    private CompanyProfileRepository companyProfileRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_save_company_profile() {
        CompanyProfile message = companyProfileRepository.save(new CompanyProfile("Xi'an", "Seven"));
        companyProfileRepository.flush();
        assertNotNull(message);
    }

    @Test
    void should_save_user_profile() {
        UserProfile message = userProfileRepository.save(new UserProfile("Xi'an", "Seven"));
        userProfileRepository.flush();
        assertNotNull(message);
    }
}
